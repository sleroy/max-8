> - Developed by [Cycling 74](https://cycling74.com)
> - Only available by downloading
> - Retail Price: 456 € VAT included
> - Reduced price reserved for individuals who are students or teachers: 288 € VAT included. Please [send us](https://shop.ircam.fr/index.php?controller=contact) a copy of your student card or a certificate from your institution
> - Ten-pack for Educational institutions - [Contact us](https://shop.ircam.fr/index.php?controller=contact)
> - [buy](https://www.ircam.fr/product/max-8/)
> - [Try max free for 30 days](https://cycling74.com/downloads)
> - Community help and [ressources](https://support.cycling74.com/hc/en-us/)

![](https://forum.ircam.fr/media/uploads/Softwares/Max8/logo_cycling74.png)

Max patching starts on a blank canvas, free of any structure. This makes it natural to build and explore unique ideas that would be too complex to make elsewhere. The Max patcher automatically expands to fit your work as it grows, no matter how much space you take up.

Max includes full-featured, expandable video and graphics tools with Jitter. Jitter is optimized for realtime audiovisual work, and is easy to combine with audio, sequencing, and modulation like everything else in Max.

Flexible access to hardware makes Max a perfect environment for prototyping. 

## [Max 8 Features](https://youtu.be/DP73qxCoCqQ ) ##

- Tap into sonic complexity with MC: with MC, patches with 100 channels of audio look as simple as those with a single channel. The ability to manipulate so many audio channels and operations.
- Noticeably faster performance: Max 8 launches between 2x for Mac and 20x faster for Windows. Large patches will load up to 4x faster. Complex UI operations are significantly more responsive.
- Simplified MIDI and keyboard control with Mappings: Max 8 includes a Mappings feature inspired by DAWs such as Ableton Live.
- Vizzie 2, completely rebuilt with OpenGL, collection of over 80 high-level modules for interactive video. It's already a perfect launchpad for learning Jitter, but now it's ready for serious use.
- Open your patches to the world with Node for Max, to build custom servers or run small applications directly from your Max patch: use NodeJS to build custom servers or run small applications directly from your Max patch. With support for the vast NPM library, Node for Max connects your patches to thousands of possibilities.
- Experience a streamlined patching workflow: Max 8 offers a more fluid and optimized patching experience. You'll notice numerous thoughtful refinements in addition to increased responsiveness (move objects into and out of patch cords in one step, manage objects in groups, filter and timestamp events with a redesigned Max console, choose a color theme to customize the Max interface…).
- Uncover the secrets to Max mastery with search.
- 37 more features including VST3 and Gen for events.

## Explore Sound without Limits ##
- Design a custom synthesizer with as many oscillators and effects as you wish,
- Manipulate samples in every way, including timestretch and pitch shifting,
- Build up more layered sounds using MC objects to patch multichannel audio.

## Generate Audio, Video, Code ##
Use Gen to patch together fine-tuned processes for audio, matrix data, or texture processing. Gen lets you combine procedural commands with visual patching to simplify the creation of custom processes. Gen objects can also output source code to be used elsewhere if needed.
Mettre lien du Gif (gen-capture.gif). Voir si fonctionne !

## [Extending Max](https://cycling74.com/products/extendmax) ##
Max’s built-in Package Manager offers immediate access to over 50 add-ons covering everything from computer vision to support for hardware controllers including Novation, Monome and ROLI controllers.
Develop custom additions in a language of your choice: Javascript, Java, NodeJS, C/C++, Gen, GLSL Shaders, Csound, Python,  Ruby, SuperCollider, Gibber, Scheme…

## What's New in Max 8.3? ##

The new features of Max 8.3 are driven by an in-depth exploration of music creation through patching. You'll find both practical utilities to solve familiar problems and new objects that will expand what you can achieve sonically with Max.

- **[New MSP Objects for Signal-Driven Music](https://youtu.be/pMXVYAtvC7s)**
- **MC improvements**: Max 8.3 improves the time-accuracy of objects like sig~ and click~ that cross between audio signals and Max events. In addition to sig~ and click~, snapshot~, edge~, peakamp~, and subdiv~ now output events more accurately in time with audio.
Large audio file support : Max 8.3 now takes full advantage of 64-bit file sizes and is no longer limited to 2GB audio files. This means you can play back and record longer and/or higher channel-count sound files.
- **Time-Accurate translations** between audio and events
- **Attrui presets**: We use attrui constantly in our patches for its convenience, but the lack of preset or pattr support meant that it was only a temporary UI solution. With 8.3, you don't have to rebuild your patches with other UI objects when you want state management for object attributes. This is a big workflow improvement for Jitter patching in particular.
- **New objects**: [crosspatch](https://docs.cycling74.com/max8/refpages/crosspatch), [matrix](https://docs.cycling74.com/max8/refpages/matrix), [mc.apply~](https://docs.cycling74.com/max8/refpages/mc.apply~), [mc.assign](https://docs.cycling74.com/max8/refpages/mc.assign), [mc.chord~](https://docs.cycling74.com/max8/refpages/mc.chord~), [mc.generate~](https://docs.cycling74.com/max8/refpages/mc.generate~), [mc.midiplayer~](https://docs.cycling74.com/max8/refpages/mc.midiplayer~), [mc.pattern~](https://docs.cycling74.com/max8/refpages/mc.pattern~), [mc.snowphasor~](https://docs.cycling74.com/max8/refpages/mc.snowphasor~), [mcs.gate~](https://docs.cycling74.com/max8/refpages/mcs.gate~),[mcs.selector~](https://docs.cycling74.com/max8/refpages/mcs.selector~), [phasegroove~](https://docs.cycling74.com/max8/refpages/phasegroove~), [ramp~](https://docs.cycling74.com/max8/refpages/ramp~), [shape~](https://docs.cycling74.com/max8/refpages/shape~), [snowfall~](https://docs.cycling74.com/max8/refpages/snowfall~https://docs.cycling74.com/max8/refpages/snowfall~), [stash~](https://docs.cycling74.com/max8/refpages/stash~), [subdiv~](https://docs.cycling74.com/max8/refpages/subdiv~), [swing~](https://docs.cycling74.com/max8/refpages/swing~), [table~](https://docs.cycling74.com/max8/refpages/table~), [twist~](https://docs.cycling74.com/max8/refpages/twist~), [updown~](https://docs.cycling74.com/max8/refpages/updown~), [what~](https://docs.cycling74.com/max8/refpages/what~), [where~](https://docs.cycling74.com/max8/refpages/where~)
- **Enriched MC Generators**: Max 8.3 brings new ways to generate values across MC signals. This includes some random generators “randomrange” and “decide" (places a 1 or 0 value in each channel), as well as the introduction of easing functions and new utility objects.

![](https://forum.ircam.fr/media/uploads/Softwares/Max8/gen-capture.gif)
>
> **[Training Courses](https://www.ircam.fr/transmission/formations-professionnelles/)**
> - [Max Initiation](https://www.ircam.fr/agenda/max-initiation-anglais-2022/detail), in English, Online Class / 24 hours of training, 8 half-days from 2:00 p.m to 5:00 p.m: November 8, 9, 15, 16, 29, 30 and December 6 & 7, 2022
>
> - [Max Initiation (session 1) / Max certification level 1](https://www.ircam.fr/agenda/max-initiation-session-1-certification-max-niveau-1-2022/detail), in French, Ircam / 36 hours of training. Monday- Saturday, November 21—26, 2023. 10am-1pm & 2:30pm-5:30pm
>
>  - [Max Initiation (session 2) / Max certification level 1](https://www.ircam.fr/agenda/max-initiation-session-2-certification-max-niveau-1-1/detail), in French, Ircam / 36 hours of training. Monday- Saturday, January 23—28, 2023. 10am-1pm & 2:30pm-5:30pm
>
> - [Max Intermediate / Max certification level 2](https://www.ircam.fr/agenda/max-perfectionnement-certification-max-niveau-2-2023/detail), in French, Ircam / 36 hours of training. Monday-Saturday, March 6-11, 2023. 10am-1pm/2:30pm-5:30pm
>
> - Max Intermdiate in English, Online  Class /24 hours of training, 8 half-days from 2:00 p.m to 5:00 p.m: April 11, 12, 19, 20 and May 2, 3, 9  & 10, 2023
>
> - [Max for Live](https://www.ircam.fr/agenda/max-for-live-2023/detail), In French, Ircam / 36 hours of training, Monday-Saturday, April 3-8, 2023. 10am-1pm/2:30pm-5:30pm
>
> **[Tutorials](https://cycling74.com/tutorials/page/1)** / **[Documentation Online](Documentation Online)**



